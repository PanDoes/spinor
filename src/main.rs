#[macro_use]
extern crate penrose;
use penrose::{
    core::{
        helpers::{
            index_selectors,
            spawn_with_args,
        },
        hooks::Hook,
        bindings::KeyEventHandler,
        manager::WindowManager,
    },
    draw::{
        bar::dwm_bar,
        TextStyle,
    },
    logging_error_handler,
    xcb::{
      new_xcb_backed_window_manager,
      XcbDraw,
    },
    Config, Forward, Backward, More, Less
};

// Settings
const BAR_FONT: &str = "azuki_font";

const BAR_HEIGHT: usize = 18;
const GAP_SIZE: u32 = 4;
const BORDER_SIZE: u32 = 2;

const  BAR_BG: u32 = 0x005b96;
const  BAR_FG: u32 = 0xffe9f8;
const MAGENTA: u32 = 0xb88cff;
const   BLACK: u32 = 0xbdc4df;
const     RED: u32 = 0xff8cb3;

// Prefered Programs
const TERMINAL: &str = "alacritty";
const LAUNCHER: &str = "dmenu_run";
const LAUNCHER_ARGS: &[&str] = &["-n"];
//const PASSWORDMANAGER: &str = "dmenu_bw";

fn main() -> penrose::Result<()> {
  // Keyboard bindings
  let bindings = gen_keybindings! {
      "M-S-q" => run_internal!(exit);

      // Client movement
      "M-S-c" => run_internal!(kill_client);
      "M-j" => run_internal!(cycle_client, Forward);
      "M-k" => run_internal!(cycle_client, Backward);

      // Layout Movement
      "M-S-h" => run_internal!(update_max_main, Less);
      "M-S-l" => run_internal!(update_max_main, More);
      "M-S-j" => run_internal!(drag_client, Forward);
      "M-S-k" => run_internal!(drag_client, Backward);

      "M-Tab" => run_internal!(toggle_workspace);

      "M-Return" => run_external!(TERMINAL); // Run Terminal
      "M-S-Return" => {{
        Box::new(move |_: &mut WindowManager<_>| {
          spawn_with_args(LAUNCHER, LAUNCHER_ARGS)
        }) as KeyEventHandler<_>
      }};

      refmap [1..7] in {
        "M-{}" => focus_workspace [ index_selectors(7) ];
        "M-S-{}" => client_to_workspace [ index_selectors(7) ];
      };
  };

  // BAR
  let workspaces = vec!["home", "web", "chat","dev", "med", "gfx", "etc"];
  let style = TextStyle {
    font: BAR_FONT.to_string(),
    point_size: 11,
    fg: BAR_FG.into(),
    bg: Some(BAR_BG.into()),
    padding: (2.0, 2.0),
  };
  let highlight = MAGENTA;
  let empty_ws = BAR_BG;
  let mut bar = dwm_bar(
    XcbDraw::new()?,
    BAR_HEIGHT,
    &style,
    highlight,
    empty_ws,
    workspaces,
  )?;


  let mut config_builder = Config::default().builder();
  let config = config_builder
      .workspaces(vec!["home", "web", "chat", "dev", "med", "gfx", "etc"])
      .border_px(BORDER_SIZE)
      .gap_px(GAP_SIZE)
      .focused_border(RED.into())
      .unfocused_border(BLACK.into())
      .build()
      .expect("Failed to build config");

  let mut wm = new_xcb_backed_window_manager(
      config, vec![], logging_error_handler())?;
  bar.startup(&mut wm)?;
  wm.grab_keys_and_run(bindings, map!{})
}

      //Workspaces
      /*map: { "home", "web", "chat",
          "dev", "med", "gfx", "etc" } to index_selectors(7) => {*/
  //run_external!("/home/pan/.fehbg &");
  //run_external!("picom --experimental-backends --backend glx -b &");
